/* global addEventListener, fetch, Request, Response, API_TOKEN, ORIGIN */

/**
 * Fetch and log a request
 * @param {Request} request
 */
async function handleRequest (request) {
  // Parse request URL to get access to query string
  const url = new URL(request.url)
  const { searchParams: query } = url
  const { pathname: path } = url

  if (path === '/favicon.ico') {
    return new Response(null, {
      status: 404,
      headers: {
        'Cache-Control': 'no-cache'
      }
    })
  }

  // Parse request headers
  const { headers } = request

  if (API_TOKEN !== 'dev' && headers.get('api-token') !== API_TOKEN) {
    return new Response(null, {
      status: 403,
      headers: {
        'Cache-Control': 'no-cache'
      }
    })
  }

  // Cloudflare-specific options are in the cf object.
  const options = {
    cf: {
      image: {
        metadata: 'none'
      }
    }
  }

  if (query.has('f')) {
    const format = query.get('f')
    if (format === 'auto') {
      if (headers.has('accept')) {
        if (headers.get('accept').includes('image/avif')) options.cf.image.format = 'avif'
        else if (headers.get('accept').includes('image/webp')) options.cf.image.format = 'webp'
      }
    } else {
      options.cf.image.format = format
    }
  }

  if (query.has('w')) options.cf.image.width = parseInt(query.get('w'))

  // Get URL of the original (full size) image to resize.
  const imageURL = ORIGIN + path

  // Build a request that passes through request headers,
  // so that automatic format negotiation can work.
  const imageRequest = new Request(imageURL, { headers })

  // Returning fetch() with resizing options will pass through response with the resized image.
  let response = await fetch(imageRequest, options)

  // Reconstruct the Response object to make its headers mutable.
  response = new Response(response.body, response)

  if (response.ok || response.status === 304) {
    // Set cache for 1 week
    response.headers.set('Cache-Control', 'max-age=604800, public')

    // Set Vary header
    response.headers.set('Vary', 'Accept')

    return response
  } else {
    return new Response(`Could not fetch the image, the server returned HTTP error ${response.status}`, {
      status: 400,
      headers: {
        'Cache-Control': 'no-cache'
      }
    })
  }
}

addEventListener('fetch', event => {
  event.respondWith(handleRequest(event.request))
})
