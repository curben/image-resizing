# Image Resizing

Simplified and query string version of Cloudflare Image Resizing using Workers.

Modified from [upstream](https://github.com/staticallyio/image-resizing)
- Support two query parameters only
- Require api token to execute

## :wrench: Requirements

- Cloudflare Images subscription
- [Wrangler](https://developers.cloudflare.com/workers/cli-wrangler/) CLI

## :zap: API

- `?w=` width
- `?f=` format
  * `webp`
  * `avif`
  * `auto` automatically serve WebP or AVIF format depending on `Accept` header

## Credit

Initial version was authored by [Frans Allen](https://upset.dev/).
